# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

def TrkToXAODSpacePointConversionCfg(flags,
                                     name: str = "TrkToXAODSpacePointConversion",
                                     **kwargs) -> ComponentAccumulator:

    acc = ComponentAccumulator()
    acc.addEventAlgo( CompFactory.InDet.TrkToXAODSpacePointConversion(name, **kwargs) )
    return acc

